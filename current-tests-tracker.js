/*
  Simple object to track data about currently running tests
*/

const CURRENT_TESTS = {};

function addToTracking(id, name) {
  const obj = {
    timestamp: new Date().getTime(),
    name,
  };

  CURRENT_TESTS[id] = obj;

  return obj;
}

function removeFromTracking(id) {
  delete CURRENT_TESTS[id];
}

module.exports = {
  CURRENT_TESTS,
  addToTracking,
  removeFromTracking,
};
