const nodemailer = require('nodemailer');
const nodemailerSendgrid = require('nodemailer-sendgrid');
const emailTemplates = require('email-templates');
const fs = require('fs');
const path = require('path');
const util = require('util');
require('dotenv').config();

const transport = nodemailer.createTransport(
  nodemailerSendgrid({
    apiKey: process.env.SENDGRID_KEY,
  }),
);

const fromEmailDetails = 'no-reply@crimecheck.in';

function eCauseListCrawlingMail(params) {
  let emailTemplate;
  const capitalizedFirstName = fromEmailDetails;
  const emailReceivers = process.env.E_CAUSELIST_EMAIL_RECIEVERS;
  const eCauseListTestingDb = process.env.E_CAUSELIST_TESTING_DB
  const mailHeading = 'E-CauseList Crawling results';
  const ejsPath = 'e-causelist-crawling';
  const {
    percentageCheck,
    crawledStateDetails,
    eCauseListDate,
  } = params;
  const subject = mailHeading;
  const toRender = ejsPath;
  const userEmail = emailReceivers;
  const email = new emailTemplates({
    views: {
      options: {
        extension: 'ejs',
      },
    },
  });
  email
    .render(toRender, {
      eCauseListTestingDb,
      percentageCheck,
      crawledStateDetails,
      eCauseListDate,
    })
    .then((result) => {
      emailTemplate = result;

      const message = {
        to: userEmail,
        from: capitalizedFirstName,
        subject,
        html: emailTemplate,
      };

      transport.sendMail(message)
        .then((sent) => {
          console.log('E-CauseList Based Crawling: email sent');
        })
        .catch((err) => {
          console.log('E-CauseList Based Crawling: error occurred', err);
        });
    })
    .catch((err) => {
      console.log('E-CauseList Based Crawling: Issue Raised in rendering the template', err);
    });
}
exports.eCauseListCrawlingMail = eCauseListCrawlingMail;

function eCauseListParsingMail(params, testId) {
  let emailTemplate;
  const capitalizedFirstName = fromEmailDetails;
  const {
    emailReceivers,
    eCauseListTestingDb,
    ejsPath,
    mailHeading,
    sheetUpdatedValues,
    gDocumentId,
    gSheetId,
  } = params;
  const subject = mailHeading;
  const toRender = ejsPath;
  const userEmail = emailReceivers;
  const email = new emailTemplates({
    views: {
      options: {
        extension: 'ejs',
      },
    },
  });
  email
    .render(toRender, {
      testId,
      link: `https://docs.google.com/spreadsheets/d/${gDocumentId}#gid=${gSheetId}`,
      eCauseListTestingDb,
      sheetUpdatedValues,
    })
    .then((result) => {
      emailTemplate = result;

      const message = {
        to: userEmail,
        from: capitalizedFirstName,
        subject,
        html: emailTemplate,
      };

      transport.sendMail(message)
        .then((sent) => {
          console.log('E-Causelist Based Parsing: email sent');
        })
        .catch((err) => {
          console.log('E-Causelist Based Parsing: error occurred', err);
        });
    })
    .catch((err) => {
      console.log('E-Causelist Based Parsing: Issue Raised in rendering the template', err);
    });
}
exports.eCauseListParsingMail = eCauseListParsingMail;
