exports.asJson = function (jsonRecord) {
  try {
    return JSON.parse(jsonRecord);
  } catch (e) {
    return false;
  }
};
  