/* eslint-disable no-console */
const fs = require('fs');
const path = require('path');
const request = require('request');
// eslint-disable-next-line import/no-extraneous-dependencies
const cheerio = require('cheerio');
const localDb = require('./database');

const constants = require('./constants');
const asJson = require('./asJson');

const database = process.env.E_CAUSELIST_TESTING_DB;
const BATCH_SIZE = 100;

function getCaseFlowDetails($, i) {
  let caseDetail = '[';
  try {
    for (let j = 1; j <= 4; j += 1) {
      const value = $(`#caseHistoryDiv > form > table.history_table.table > tbody > tr:nth-child(${i}) > td:nth-child(${j})`).text();
      caseDetail += `${value.trim()}`;
      if (j !== 4) {
        caseDetail += ',';
      }
    }
    caseDetail += ']';
    return caseDetail;
  } catch (error) {
    return '[]';
  }
}

function getCaseFlow($) {
  let caseFlow = '[[]';
  try {
    const totalRows = $('#caseHistoryDiv > form > table.history_table.table > tbody > tr');
    for (let i = 1; i <= totalRows.length; i += 1) {
      if (i !== 0) {
        caseFlow += ',';
      }
      const caseDetail = getCaseFlowDetails($, i);
      caseFlow += caseDetail;
    }
    caseFlow += ']';
    return caseFlow;
  } catch (error) {
    return '[]';
  }
}

function generateCaseTypeNoMap($) {
  try {
    const caseTypeNoMap = new Map();
    const caseTypeNameCols = $('#SubordinateCourtFilingCaseType > option');
    for (let k = 1; k < caseTypeNameCols.length; k += 1) {
      const caseTypeName = $(`#SubordinateCourtFilingCaseType > option:nth-child(${k + 1})`).text();
      const value = $(`#SubordinateCourtFilingCaseType > option:nth-child(${k + 1})`).attr('value');
      caseTypeNoMap.set(caseTypeName.toLowerCase().replace(/\s/g, ''), value);
    }
    return caseTypeNoMap;
  } catch (error) {
    const message = 'E-CauseList Based Parsing: #SubordinateCourtFilingCaseType -> Can not get caseTypeNo Mappings -> Reloading... ';
    throw new Error(message);
  }
}

function parseHtmlFile(
  fileName,
  id,
  parsedRecordValues,
  parsedRecords,
  parsingErrorRecords,
  missingFiles,
  crawledRecords,
) {
  return new Promise((resolve, reject) => {
    if (!fileName) {
      reject();
    }
    const record = {};
    const subRec = [];
    fs.readFile(path.join(__dirname, `html-contents/${fileName}`), 'utf8', (error, data) => {
      if (error) {
        if (error.message.includes('no such file or directory')) {
          console.log(`E-CauseList Based Parsing: ${fileName} is missing`);
          missingFiles.push(id);
        } else {
          parsingErrorRecords.push(id);
        }
      } else {
        const $ = cheerio.load(data);
        const caseTypeNoMap = generateCaseTypeNoMap($);
        let caseName = '';

        const cinnumber = $('table > tbody > tr > td > span').text();

        if (fileName.replace('.html', '') !== cinnumber) {
          console.log(`E-CauseList Based Parsing: Crawling Error for ${fileName}`);
          parsingErrorRecords.push(id);
        } else {
          // regDate
          const regDate = $('#shareSelect > table.table.case_details_table > tbody > tr:nth-child(3) > td:nth-child(4) > label').text();
          let caseYear;
          if (regDate) {
            caseYear = regDate.substr(-4);
          }

          const caseTypeName = $('#shareSelect > table.table.case_details_table > tbody > tr:nth-child(1) > td:nth-child(2)').text();

          let filingNumber = $('#shareSelect > table.table.case_details_table > tbody > tr:nth-child(2) > td:nth-child(2)').text();
          if (filingNumber) {
            filingNumber = filingNumber.trim();
          }

          let filingDate = $('#shareSelect > table.table.case_details_table > tbody > tr:nth-child(2) > td:nth-child(4)').text();
          if (filingDate) {
            filingDate = filingDate.trim();
          }

          const regNumber = $('#shareSelect > table.table.case_details_table > tbody > tr:nth-child(3) > td:nth-child(2) > label').text();

          if (caseYear === '' && filingDate) {
            caseYear = filingDate.substr(-4);
          }
          if (caseYear === '') {
            caseYear = cinnumber.substr(-4);
          }
          if (caseYear) {
            caseYear = caseYear.trim();
          }
          const stateDistCode = $('#shareSelect > table.table.case_details_table > tbody > tr:nth-child(4) > td:nth-child(3) > a').attr('onclick');
          const stateDistCodeArray = stateDistCode.split('&');
          let stateCode = stateDistCodeArray[1];
          let distCode = stateDistCodeArray[2];
          if (stateCode) {
            stateCode = stateCode.substr(stateCode.indexOf('=') + 1);
          }
          if (distCode) {
            distCode = distCode.substr(distCode.indexOf('=') + 1);
          }
          const petitioneraddress = $('table[class = "table_o table  Petitioner_Advocate_table"] > tbody > tr > td').text();

          const respondentaddress = $('table[class = "table_o table  Respondent_Advocate_table"] > tbody > tr > td').text();

          let caseNo = '';
          let courtNumber = '';
          let respondent = '';
          let petitioner = '';
          let litigantValue = '';
          let versusIndex = '';
          const casess = $('#pageCLDiv > form > div:nth-child(7) > table > tbody > tr');

          let caseHeader;
          for (let i = 0; i <= casess.length; i += 1) {
            const headers = $(`#pageCLDiv > form > div:nth-child(7) > table > tbody > tr:nth-child(${i + 1}) > td`);
            if (headers.length === 1) {
              caseHeader = $(`#pageCLDiv > form > div:nth-child(7) > table > tbody > tr:nth-child(${i + 1}) > td`).text();
            }
            const viewHistory = $(`#pageCLDiv > form > div:nth-child(7) > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(2) > a`).attr('onclick');
            if (viewHistory && viewHistory.includes(cinnumber)) {
              litigantValue = $(`#pageCLDiv > form > div:nth-child(7) > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(3)`).text();
              versusIndex = litigantValue.indexOf('versus');
              caseName = $(`#pageCLDiv > form > div:nth-child(7) > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(2) > a`).text();
              let arr = [];
              arr = viewHistory.split(',');
              [, caseNo,, courtNumber] = arr;
              caseNo = caseNo.replace(/'/g, '');
              courtNumber = courtNumber.substr(0, courtNumber.length - 1).replace(/'/g, '');
              break;
            }
          }
          /* If versus is not present in litigant values, validating those from their address */
          if (versusIndex !== -1) {
            petitioner = litigantValue.substr(0, versusIndex).trim();
            respondent = litigantValue.substr(versusIndex + 6).trim();
          } else if (respondentaddress.includes(litigantValue)) {
            respondent = litigantValue;
          } else {
            petitioner = litigantValue;
          }

          const caseTypeNo = caseTypeNoMap.get(caseTypeName.toLowerCase().replace(/\s/g, ''));

          const policeStation = $('table[class = "FIR_details_table table  table_o"] > tbody > tr:nth-child(1) > td:nth-child(2) > label').text();

          const firNumber = $('table[class = "FIR_details_table table  table_o"] > tbody > tr:nth-child(2) > td:nth-child(2) > label').text();

          const firYear = $('table[class = "FIR_details_table table  table_o"] > tbody > tr:nth-child(3) > td:nth-child(2) > label').text();

          const decisionDate = $('table[class = "Lower_court_table table "] > tbody > tr:nth-child(3) > td:nth-child(2)').text();

          const courtName = $('#chHeading').text();

          const rows = $('table[class = "table_r table  text-left"] > tbody > tr');
          const courtNumberAndJudge = $(`#shareSelect > table.table_r.table.text-left > tbody > tr:nth-child(${rows.length}) > td:nth-child(2) > label > strong`).text();

          const caseStage = $('#shareSelect > table.table_r.table.text-left > tbody > tr:nth-child(3) > td:nth-child(2) > label > strong').text();

          const caseStatus = $('#shareSelect > table.table_r.table.text-left > tbody > tr:nth-child(3) > td:nth-child(2) > strong').text();

          const underSection = $('#act_table > tbody > tr:nth-child(2) > td:nth-child(2)').text();

          const hearingDate = $('#shareSelect > table.table_r.table.text-left > tbody > tr:nth-child(1) > td:nth-child(2) > strong').text();

          const underact = $('#act_table > tbody > tr:nth-child(2) > td:nth-child(1)').text();

          const classification = 'Criminal';
          const caseFlow = getCaseFlow($);
          subRec.push(new Date().toISOString().slice(0, 19).replace('T', ' '));
          subRec.push(cinnumber || '');
          record.cinnumber = cinnumber || '';
          subRec.push(caseNo || '');
          record.caseNo = caseNo || '';
          subRec.push(courtNumber || '');
          record.courtNumber = courtNumber || '';
          subRec.push(caseName || '');
          record.caseName = caseName || '';
          subRec.push(regDate || '');
          record.regDate = regDate || '';
          subRec.push(caseTypeName || '');
          record.caseTypeName = caseTypeName || '';
          subRec.push(caseTypeNo || '');
          record.caseTypeNo = caseTypeNo || '';
          subRec.push(filingNumber || '');
          record.filingNumber = filingNumber || '';
          subRec.push(filingDate || '');
          record.filingDate = filingDate || '';
          subRec.push(regNumber || '');
          record.regNumber = regNumber || '';
          subRec.push(caseFlow || '');
          record.caseFlow = caseFlow || '';
          subRec.push(petitioneraddress || '');
          record.petitioneraddress = petitioneraddress || '';
          subRec.push(respondentaddress || '');
          record.respondentaddress = respondentaddress || '';
          subRec.push(petitioner || '');
          record.petitioner = petitioner || '';
          subRec.push(respondent || '');
          record.respondent = respondent || '';
          subRec.push(decisionDate || '');
          record.decisionDate = decisionDate || '';
          subRec.push(courtNumberAndJudge || '');
          record.courtNumberAndJudge = courtNumberAndJudge || '';
          subRec.push(caseStage || '');
          record.caseStage = caseStage || '';
          subRec.push(caseStatus || '');
          record.caseStatus = caseStatus || '';
          subRec.push(caseHeader || '');
          record.caseHeader = caseHeader || '';
          subRec.push(underSection || '');
          record.underSection = underSection || '';
          subRec.push(policeStation || '');
          record.policeStation = policeStation || '';
          subRec.push(firNumber || '');
          record.firNumber = firNumber || '';
          subRec.push(courtName || '');
          record.courtName = courtName || '';
          subRec.push(hearingDate || '');
          record.hearingDate = hearingDate || '';
          subRec.push(underact || '');
          record.underact = underact || '';
          subRec.push(firYear || '');
          record.firYear = firYear || '';
          subRec.push(caseYear || '');
          record.caseYear = caseYear || '';
          subRec.push(stateCode || '');
          record.stateCode = stateCode || '';
          subRec.push(distCode || '');
          record.distCode = distCode || '';
          subRec.push(classification || '');
          record.classification = classification;
          crawledRecords.push(record);
          parsedRecordValues.push(subRec);
          parsedRecords.push(id);
        }
      }
      resolve();
    });
  });
}

// Searching records through cinnumber in `crime_index`
function generateEsQuery(records) {
  const query = [];
  for (let i = 0; i < records.length; i += 1) {
    query.push({ index: 'crime_index' });
    query.push({
      query: {
        bool: {
          must: [
            {
              multi_match: {
                query: records[i][1],
                fields: ['cinnumber.keyword'],
              },
            },
          ],
        },
      },
    });
  }
  return query;
}

async function bulkSearch(query, dashboard) {
  return new Promise((resolve, reject) => {
    if (query.length === 0) {
      console.log('E-CauseList Based Parsing: bulkDataSearch Rejected');
      reject();
      return;
    }
    const dashboardValue = constants.getDashBoardValue(dashboard);
    request.get({
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        Authorization: `Bearer ${constants.getEsProxyApiKey(dashboardValue)}`,
      },
      url: constants.getHostUrl(dashboardValue),
      form: {
        searchType: 'msearch',
        index: 'crime_index',
        query,
      },
    }, (err, response, data) => {
      const resp = asJson.asJson(data);
      if (err || !resp) {
        console.error(`Data Check (bulkDataSearch) ${dashboardValue}  ${err}`);
        reject();
        return;
      }
      const allData = resp.responses;

      if (allData !== undefined) {
        resolve(allData);
      } else {
        console.log(resp);
        console.log('E-CauseList Based Parsing: Log: ----- ERROR -----');
        reject();
      }
    });
  });
}

async function searchInEs(parsedRecordValues, dashboard) {
  const query = generateEsQuery(parsedRecordValues);
  if (query.length !== 0) {
    const esRecords = await bulkSearch(query, dashboard);
    return esRecords;
  }
  return [];
}

function compareLitigantAddress(cinnumber, esLitigant, crawledLitigant, updateBool, key) {
  let esLitigantValue = esLitigant;
  if (!esLitigantValue) {
    esLitigantValue = '';
  }
  if ((crawledLitigant.trim()
            && !esLitigantValue.trim())
            || (crawledLitigant.trim()
            && crawledLitigant.trim().length
            > esLitigantValue.trim().length)) {
    if (!updateBool) {
      console.log(`E-CauseList Based Parsing: Updation required for Cinnumber: ${cinnumber}`);
    }
    console.log(`E-CauseList Based Parsing: ${key}`);
    return true;
  }
  return updateBool;
}

function compareValues(cinnumber, key, crawledValue, esRecValue, updateBool) {
  let esValue = esRecValue;
  if (!esValue) {
    esValue = '';
  }
  if (typeof esValue === 'string') {
    esValue = esValue.trim();
  }
  if ((crawledValue
                && crawledValue.trim() !== esValue)) {
    if (!updateBool) {
      console.log(`E-CauseList Based Parsing: Updation required for Cinnumber: ${cinnumber}`);
    }
    console.log(`E-CauseList Based Parsing: ${key}`);
    return true;
  }
  return updateBool;
}

function getMissingModifiedData(crawledRecords, recordValues, esRecords) {
  const requiredValues = {};
  requiredValues.totalCases = 0;
  requiredValues.missingCases = 0;
  requiredValues.modifiedCases = 0;

  for (let i = 0; i < esRecords.length; i += 1) {
    const esData = esRecords[i];
    const esHits = esData.hits.hits;
    requiredValues.totalCases += 1;
    if (esHits.length === 0) {
      // if missing case is there in ES
      console.log(`E-CauseList Based Parsing: data is missing for ${crawledRecords[i].cinnumber}`);
      recordValues[i].push('2');
      requiredValues.missingCases += 1;
    } else {
      let updateBool = false;
      // Iterating over all crawled fields
      Object.keys(crawledRecords[i]).forEach((key) => {
        if (key === 'respondentaddress' || key === 'petitioneraddress') {
          updateBool = compareLitigantAddress(
            // eslint-disable-next-line no-underscore-dangle
            esHits[0]._source.cinnumber,
            // eslint-disable-next-line no-underscore-dangle
            esHits[0]._source[key.toLowerCase()],
            crawledRecords[i][key],
            updateBool,
            key,
          );
        } else if (key !== 'cinnumber' && key !== 'classification' && key !== 'caseHeader') {
          updateBool = compareValues(
            // eslint-disable-next-line no-underscore-dangle
            esHits[0]._source.cinnumber,
            key,
            crawledRecords[i][key],
            // eslint-disable-next-line no-underscore-dangle
            esHits[0]._source[key.toLowerCase()],
            updateBool,
          );
        }
      });
      if (updateBool) {
        // in case: if case details are modified
        recordValues[i].push('3');
        requiredValues.modifiedCases += 1;
      } else {
        recordValues[i].push('1');
      }
    }
    console.log('E-CauseList Based Parsing: --');
  }
  return requiredValues;
}

async function parseFiles(toParse, dashboard, tableName, fileNameTable) {
  const parsedRecords = [];
  const parsedRecordValues = [];
  const parsingErrorRecords = [];
  const missingFiles = [];
  const crawledRecords = [];
  for (let j = 0; j < toParse.length; j += 1) {
    const { fileName, id } = toParse[j];

    // eslint-disable-next-line no-await-in-loop
    await parseHtmlFile(
      fileName,
      id,
      parsedRecordValues,
      parsedRecords,
      parsingErrorRecords,
      missingFiles,
      crawledRecords,
    );
    console.log(`E-CauseList Based Parsing: ${fileName} parsed`);
  }
  const esRecords = await searchInEs(parsedRecordValues, dashboard);
  const courtSummary = getMissingModifiedData(crawledRecords, parsedRecordValues, esRecords);
  if (parsedRecordValues.length > 0) {
    const query = `insert into ${database}.${tableName} (date, cinNumber, caseNo, courtNumber, caseName, regDate, caseTypeName, caseTypeNo, filingNumber, filingDate, regNumber, caseFlow , petitionerAddress, respondentAddress, petitioner, respondent, decisionDate, courtNumberAndJudge, caseStage, status, caseHeader, underSection, policeStation, firNumber, courtName, hearingDate, underact, firYear, caseYear, stateCode, distCode, classification, recordFound) values ?`;
    // eslint-disable-next-line no-await-in-loop
    await localDb.insertMultipleRows(query, parsedRecordValues);
    console.log('E-CauseList Based Parsing: Records inserted');
  }

  if (parsedRecords.length > 0) {
    const query = `update ${database}.${fileNameTable} set parsed = 2 where id IN (${parsedRecords.join(',')})`;
    // eslint-disable-next-line no-await-in-loop
    await localDb.executeQuerySync(query);
  }
  if (parsingErrorRecords.length > 0) {
    const query = `update ${database}.${fileNameTable} set parsed = 4 where id IN (${parsingErrorRecords.join(',')})`;
    // eslint-disable-next-line no-await-in-loop
    await localDb.executeQuerySync(query);
  }
  if (missingFiles.length > 0) {
    const query = `update ${database}.${fileNameTable} set parsed = 3 where id IN (${missingFiles.join(',')})`;
    // eslint-disable-next-line no-await-in-loop
    await localDb.executeQuerySync(query);
  }
  return courtSummary;
}

async function initParsing(dashboard) {
  const allTables = process.env.E_CAUSELIST_PARSING_STATES_CSV;
  const tableNames = allTables.split(',');
  const parsedSummary = [];
  for (let tbl = 0; tbl < tableNames.length; tbl += 1) {
    const tableName = `${tableNames[tbl]}_crawl`;
    const table = tableName.replace('_crawl', '');
    const sheetUpdatedValues = {};
    sheetUpdatedValues.missingCases = 0;
    sheetUpdatedValues.totalCases = 0;
    sheetUpdatedValues.modifiedCases = 0;
    sheetUpdatedValues.missingRecordsTable = 'N/A';
    sheetUpdatedValues.modifiedRecordsTable = 'N/A';
    sheetUpdatedValues.tableName = table;

    let query = `select * from ${database}.${tableName} where parsed=1`;
    // eslint-disable-next-line no-await-in-loop
    const toParse = await localDb.executeQuerySync(query);

    query = `create table IF NOT EXISTS ${database}.${table} (id int NOT NULL AUTO_INCREMENT,date DATETIME,cinNumber varchar(100),regDate varchar(100), caseYear varchar(100), caseTypeName varchar(200), filingNumber varchar(200),filingDate varchar(100), regNumber varchar(200), stateCode varchar(100), distCode varchar(100), caseFlow TEXT, petitionerAddress TEXT, respondentAddress TEXT, petitioner TEXT, respondent TEXT, caseName varchar (200), caseTypeNo varchar (200), caseNo varchar (200), courtNumber varchar (200), policeStation TEXT, firNumber varchar(500), firYear varchar(500),decisionDate varchar(500), courtName varchar(500), recordFound enum('found', 'missing', 'modified'), courtNumberAndJudge varchar(500), caseStage varchar(500), status varchar(500), caseHeader varchar(500), underSection varchar(500), hearingDate varchar(500), underAct varchar(500), caseStatus varchar(500), classification varchar(500) DEFAULT 'Criminal', caseLink varchar(200) DEFAULT 'https://services.ecourts.gov.in/ecourtindia_v6/', courtType varchar(500) DEFAULT 'District Court', PRIMARY KEY (id))`;
    // eslint-disable-next-line no-await-in-loop
    await localDb.executeQuerySync(query);

    query = `select id from ${database}.${table} ORDER BY ID DESC LIMIT 1`;
    // eslint-disable-next-line no-await-in-loop
    let lastId = await localDb.executeQuerySync(query);

    if (lastId.length === 0) {
      lastId = 0;
    } else {
      lastId = lastId[0].id;
    }
    sheetUpdatedValues.lastId = lastId;
    console.log(`E-CauseList Based Parsing: Total Records to parse ${table} - ${toParse.length}`);
    for (let i = 0; i < toParse.length; i += BATCH_SIZE) {
      // eslint-disable-next-line no-await-in-loop
      const requiredValues = await parseFiles(
        toParse.slice(i, i + BATCH_SIZE),
        dashboard,
        table,
        tableName,
      );
      sheetUpdatedValues.missingCases += requiredValues.missingCases;
      sheetUpdatedValues.totalCases += requiredValues.totalCases;
      sheetUpdatedValues.modifiedCases += requiredValues.modifiedCases;
    }
    if (sheetUpdatedValues.missingCases > 0) {
      const missingRecTable = `${table}_Missing`;
      query = `create table IF NOT EXISTS ${database}.${missingRecTable} (id int NOT NULL AUTO_INCREMENT,date DATETIME,cinNumber varchar(100),regDate varchar(100), caseYear varchar(100), caseTypeName varchar(200), filingNumber varchar(200),filingDate varchar(100), regNumber varchar(200), stateCode varchar(100), distCode varchar(100), caseFlow TEXT, petitionerAddress TEXT, respondentAddress TEXT, petitioner TEXT, respondent TEXT, caseName varchar (200), caseTypeNo varchar (200), caseNo varchar (200),courtNumber varchar (200), policeStation TEXT, firNumber varchar(500), firYear varchar(500),decisionDate varchar(500), courtName varchar(500), courtNumberAndJudge varchar(500), caseStage varchar(500), status varchar(500), caseHeader varchar(500),underSection varchar(500), hearingDate varchar(500), underAct varchar(500), caseStatus varchar(500), classification varchar(500) DEFAULT 'Criminal', caseLink varchar(200) DEFAULT 'https://services.ecourts.gov.in/ecourtindia_v6/', courtType varchar(500) DEFAULT 'District Court', PRIMARY KEY (id))`;
      // eslint-disable-next-line no-await-in-loop
      await localDb.executeQuerySync(query);
      query = `select id from ${database}.${missingRecTable} ORDER BY ID DESC LIMIT 1`;
      // eslint-disable-next-line no-await-in-loop
      let missingTableLastid = await localDb.executeQuerySync(query);
      if (missingTableLastid.length === 0) {
        missingTableLastid = 0;
      } else {
        missingTableLastid = missingTableLastid[0].id;
      }
      sheetUpdatedValues.missingTableLastid = missingTableLastid;
      query = `insert into ${database}.${missingRecTable} (date, cinNumber,regDate, caseYear, caseTypeName, filingNumber,filingDate, regNumber, stateCode, distCode, caseFlow, petitionerAddress, respondentAddress, petitioner, respondent, caseName, caseTypeNo, caseNo, courtNumber, policeStation, firNumber, firYear,decisionDate, courtName, courtNumberAndJudge, caseStage, status, caseHeader, underSection, hearingDate, underAct) select date, cinNumber,regDate, caseYear, caseTypeName, filingNumber,filingDate, regNumber, stateCode, distCode, caseFlow, petitionerAddress, respondentAddress, petitioner, respondent, caseName, caseTypeNo, caseNo, courtNumber, policeStation, firNumber, firYear,decisionDate, courtName, courtNumberAndJudge, caseStage, status, caseHeader, underSection, hearingDate, underAct from ${database}.${table} where recordFound=2 and id>${lastId};`;
      // eslint-disable-next-line no-await-in-loop
      await localDb.executeQuerySync(query);
      sheetUpdatedValues.missingRecordsTable = missingRecTable;
    }
    if (sheetUpdatedValues.modifiedCases > 0) {
      const modifiedRecTable = `${table}_Modified`;
      query = `create table IF NOT EXISTS ${database}.${modifiedRecTable} (id int NOT NULL AUTO_INCREMENT, date DATETIME,cinNumber varchar(100),regDate varchar(100), caseYear varchar(100), caseTypeName varchar(200), filingNumber varchar(200),filingDate varchar(100), regNumber varchar(200), stateCode varchar(100), distCode varchar(100), caseFlow TEXT, petitionerAddress TEXT, respondentAddress TEXT, petitioner TEXT, respondent TEXT, caseName varchar (200), caseTypeNo varchar (200), caseNo varchar (200),courtNumber varchar (200), policeStation TEXT, firNumber varchar(500), firYear varchar(500),decisionDate varchar(500), courtName varchar(500), courtNumberAndJudge varchar(500), caseStage varchar(500), status varchar(500), caseHeader varchar(500), underSection varchar(500), hearingDate varchar(500), underAct varchar(500), caseStatus varchar(500), classification varchar(500) DEFAULT 'Criminal', caseLink varchar(200) DEFAULT 'https://services.ecourts.gov.in/ecourtindia_v6/', courtType varchar(500) DEFAULT 'District Court', PRIMARY KEY (id))`;
      // eslint-disable-next-line no-await-in-loop
      await localDb.executeQuerySync(query);
      query = `select id from ${database}.${modifiedRecTable} ORDER BY ID DESC LIMIT 1`;
      // eslint-disable-next-line no-await-in-loop
      let modifiedTableLastid = await localDb.executeQuerySync(query);
      if (modifiedTableLastid.length === 0) {
        modifiedTableLastid = 0;
      } else {
        modifiedTableLastid = modifiedTableLastid[0].id;
      }
      sheetUpdatedValues.modifiedTableLastid = modifiedTableLastid;
      query = `insert into ${database}.${modifiedRecTable} (date, cinNumber,regDate, caseYear, caseTypeName, filingNumber,filingDate, regNumber, stateCode, distCode, caseFlow, petitionerAddress, respondentAddress, petitioner, respondent, caseName, caseTypeNo, caseNo, courtNumber, policeStation, firNumber, firYear,decisionDate, courtName, courtNumberAndJudge, caseStage, status, caseHeader, underSection, hearingDate, underAct) select date, cinNumber,regDate, caseYear, caseTypeName, filingNumber,filingDate, regNumber, stateCode, distCode, caseFlow, petitionerAddress, respondentAddress, petitioner, respondent, caseName, caseTypeNo, caseNo, courtNumber, policeStation, firNumber, firYear,decisionDate, courtName, courtNumberAndJudge, caseStage, status, caseHeader, underSection, hearingDate, underAct from ${database}.${table} where recordFound=3 and id>${lastId};`;
      // eslint-disable-next-line no-await-in-loop
      await localDb.executeQuerySync(query);
      sheetUpdatedValues.modifiedRecordsTable = modifiedRecTable;
    }
    if (sheetUpdatedValues.missingCases === 0) {
      sheetUpdatedValues.missingCasesPerc = 0;
    } else {
      sheetUpdatedValues.missingCasesPerc = (sheetUpdatedValues.missingCases
        / sheetUpdatedValues.totalCases) * 100;
    }
    sheetUpdatedValues.missingCasesPerc = sheetUpdatedValues.missingCasesPerc.toFixed(2);
    parsedSummary.push(sheetUpdatedValues);
  }
  return parsedSummary;
}

module.exports.initParsing = initParsing;
