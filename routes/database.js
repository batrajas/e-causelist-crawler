const mysql = require('mysql');
const util = require('util');
require('dotenv').config();

const E_CAUSELIST_TESTING_DB = process.env.E_CAUSELIST_TESTING_DB || '';
const dbUsername = process.env.DB_USERNAME || '';
const dbPassword = process.env.DB_PASSWORD || '';

const dbPool = mysql.createPool({
  user: dbUsername,
  password: dbPassword,
  database: E_CAUSELIST_TESTING_DB,
});

const executeQuerySync = async (query) => {
  const result = await util.promisify(dbPool.query).call(dbPool, query);
  return result;
};
const insertMultipleRows = async (query, values) => {
  await util.promisify(dbPool.query).call(dbPool, query, [values]);
};

module.exports = {
  executeQuerySync,
  insertMultipleRows,
};
