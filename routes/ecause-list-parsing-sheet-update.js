const {
  google,
} = require('googleapis');
require('express');
require('../utils/my_libs');
const myModules = require('../utils/my_modules');

const myConstants = myModules.constants;
// eslint-disable-next-line import/no-dynamic-require
const keys = require(`${myConstants.path.json}/drive.json`);

const sheetId = process.env.E_CAUSELIST_TEST_DOCUMENT_ID;
const addressTestSheet = process.env.E_CAUSELIST_TEST_SHEET_NAME;
const addressTestSheetId = process.env.E_CAUSELIST_TEST_SHEET_ID;

const client = new google.auth.JWT(
  keys.client_email,
  null,
  keys.private_key,
  ['https://www.googleapis.com/auth/spreadsheets'],
);

let gsapi = null;
async function gsrun(cl) {
  gsapi = google.sheets({
    version: 'v4',
    auth: cl,
  });
}
gsapi = google.sheets({
  version: 'v4',
  auth: client,
});

client.authorize((err) => {
  if (err) {
    console.log(err);
  } else {
    console.log('connected..');
    gsrun(client);
  }
});

const addHeader = async (testId) => {
  const range = ['B1'];
  const arrayData = [[testId]];
  for (let i = 0; i <= 0; i += 1) {
    const upOpt = {
      spreadsheetId: sheetId,
      resource: {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: `${addressTestSheet}!${range[i]}`,
          majorDimension: 'COLUMNS',
          values: [
            arrayData[i],
          ],
        }],
      },
    };
    // eslint-disable-next-line no-await-in-loop
    await gsapi.spreadsheets.values.batchUpdate(upOpt);
  }
  return 'Header added!';
};

async function insertColumn(range, arrayData, type) {
  const upOpt = {
    spreadsheetId: sheetId,
    resource: {
      valueInputOption: 'USER_ENTERED',
      data: [{
        range: `${addressTestSheet}!${range}`,
        majorDimension: type,
        values: [
          arrayData,
        ],
      }],
    },
  };
  await gsapi.spreadsheets.values.batchUpdate(upOpt);
}

const createColumn = async () => {
  const firstColumns = 1;
  const totalTestResultColumns = 1;

  const batchUpdateRequest = {
    requests: [{
      insertDimension: {
        range: {
          sheetId: addressTestSheetId,
          dimension: 'COLUMNS',
          startIndex: firstColumns,
          endIndex: firstColumns + totalTestResultColumns,
        },
        inheritFromBefore: false,
      },
    }],
  };

  await gsapi.spreadsheets.batchUpdate({
    spreadsheetId: sheetId,
    resource: batchUpdateRequest,
  }).then(() => 'Column created');
};

const insertData = async (sheetUpdatedValues, eCauseListTestingDb) => {
  insertColumn('B2', [sheetUpdatedValues.tableName], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B3', [`${eCauseListTestingDb}/${sheetUpdatedValues.tableName}`], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B4', [sheetUpdatedValues.totalCases], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B5', [sheetUpdatedValues.lastId.toString()], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B6', [`${eCauseListTestingDb}/${sheetUpdatedValues.missingRecordsTable}`], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B7', [sheetUpdatedValues.missingCases], 'ROWS').catch((err) => {
    console.log(err);
  });

  insertColumn('B8', [sheetUpdatedValues.missingCasesPerc], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B9', [sheetUpdatedValues.missingTableLastid], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B10', [`${eCauseListTestingDb}/${sheetUpdatedValues.modifiedRecordsTable}`], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B11', [sheetUpdatedValues.modifiedCases], 'ROWS').catch((err) => {
    console.log(err);
  });
  insertColumn('B12', [sheetUpdatedValues.modifiedTableLastid], 'ROWS').catch((err) => {
    console.log(err);
  });
};

const exportDataToDrive = async function exportDataToDrive(testId,
  sheetUpdatedValues,
  eCauseListTestingDb) {
  console.log(sheetUpdatedValues);
  for (let i = 0; i < sheetUpdatedValues.length; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    await createColumn();
    // eslint-disable-next-line no-await-in-loop
    await addHeader(testId);
    // eslint-disable-next-line no-await-in-loop
    await insertData(sheetUpdatedValues[i], eCauseListTestingDb);
  }
  console.log('E-CauseList Based Parsing: Data Inserted in sheet');
};

module.exports.exportDataToDrive = exportDataToDrive;
