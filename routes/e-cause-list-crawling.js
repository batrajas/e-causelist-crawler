/* eslint-disable no-console */
/*
  Dependencies:
  1. ecourt_court_list (Active=1 for crawling) table should exist in same database.
  2. Install imagemagick & tesseract to break captcha.
*/
const puppeteer = require('puppeteer');
const { Cluster } = require('puppeteer-cluster');
const cmd = require('node-cmd');
const fs = require('fs');
const path = require('path');
const constants = require('./constants');
const sendMail = require('./sendMail');

require('events').EventEmitter.defaultMaxListeners = 15;
// require('events').EventEmitter.prototype._maxListeners = 100;
// require('events').EventEmitter.prototype._maxListeners = 0;

const localDb = require('./database');

// After this count, it will go to sleep
let reloadCount = parseInt(process.env.E_CAUSELIST_RELOAD_COUNT, 10) || 100;
const initialReloadCount = reloadCount;

let maxReloadCount = parseInt(process.env.E_CAUSELIST_MAX_RELOAD_COUNT, 10) || 1000;
// in m/s
let reloadCounts = new Map();
let maxReloadCounts = new Map();
let crawlingLastIdDetails = new Map();
let stateTotalCases = new Map();

const reloadSleepTimer = parseInt(process.env.E_CAUSELIST_RELOAD_SLEEP_TIME, 10) || 3700;

const database = process.env.E_CAUSELIST_TESTING_DB;
let cluster;

// Breaking Captcha
function runInCommandLine(i) {
  return new Promise((resolve, reject) => {
    if (!i || i === '') {
      reject();
    }
    let command = `convert ${i} -fuzz 2% -fill black -opaque "#707070" -threshold 1% -alpha off -fill "#707070" -opaque black out.png;compare ${i} out.png -highlight-color black diff.png; convert diff.png -fill white -fuzz 10% +opaque "#000000" -negate -verbose -morphology Thinning 3:0,0,0,0,1,0,0,0,0 -negate -morphology close Diamond ${i}; tesseract -l eng --psm 8 ${i} captcha;`;

    // eslint-disable-next-line no-unused-vars
    cmd.get(command, ((_err, _data, _stderr) => {
      command = 'cat captcha.txt;';
      // eslint-disable-next-line no-unused-vars
      cmd.get(command, ((err, data, stderr) => {
        resolve(data);
      }));
    }));
  });
}

/*
  Captcha Image Selector: #captcha_image
  Saving Captcha Image as: secureimage.png
*/
async function getCaptcha(page) {
  let captchaLogo;
  let captchaImageName;
  try {
    captchaLogo = await page.$('#captcha_image');
    captchaImageName = 'secureimage.png';
    captchaLogo.screenshot({
      path: captchaImageName,
    });
  } catch (error) {
    throw new Error(error);
  }
  let caseCaptcha = '';
  const captchaText = await runInCommandLine(captchaImageName);
  caseCaptcha = captchaText.replace(/[^a-zA-Z0-9]/g, '').trim();
  console.log(`E-CauseList Based Crawling: Captcha is ${caseCaptcha}`);
  return caseCaptcha;
}

async function waitingForSelector(page, selector, message, timeout) {
  try {
    await page.waitForSelector(selector, { timeout });
  } catch (error) {
    throw new Error(message);
  }
}

async function pageReload(page, message, workerId) {
  let currentReloadCount = reloadCounts.get(workerId);
  let currentMaxReloadCount = maxReloadCounts.get(workerId);
  currentReloadCount -= 1;
  currentMaxReloadCount -= 1;
  reloadCounts.set(workerId, currentReloadCount);
  maxReloadCounts.set(workerId, currentMaxReloadCount);
  console.log(message);
  console.log(`E-CauseList Based Crawling: ${workerId}- maxReloadCount: ${currentMaxReloadCount}`);
  if (!currentMaxReloadCount) {
    throw new Error('Max reload count reached... cannot proceed further');
  }
  if (!currentReloadCount) {
    console.log(`E-CauseList Based Crawling: ${workerId}- Reload count exceeded, going to sleep for 10 minutes...`);
    await page.waitFor(600000); // 10 min
    currentReloadCount = initialReloadCount;
    reloadCounts.set(workerId, currentReloadCount);
  }
  await page.waitFor(reloadSleepTimer);
  try {
    await page.reload();
  } catch (error) {
    console.log(error);
    // If in non-headless mode we closed the browser
    if (error.message.includes('Session closed. Most likely the page has been closed.')) {
      throw new Error('Process Stopped cannot proceed further');
    }
  }
}

async function selectCourtComplex(page, courtId) {
  let selected = false;
  let courtComplexCode = '';
  const courtComplexCodes = await page.$$('#court_complex_code > option');
  try {
    for (let code = 0; code < courtComplexCodes.length; code += 1) {
      if (selected) {
        break;
      }

      // eslint-disable-next-line no-await-in-loop
      await page.waitForSelector(`#court_complex_code > option:nth-child(${code + 1})`, { timeout: 1800 });

      // eslint-disable-next-line no-await-in-loop
      const courtComplexOption = await page.$(`#court_complex_code > option:nth-child(${code + 1})`);

      // eslint-disable-next-line no-await-in-loop
      const courtComplexOptionValue = await courtComplexOption.getProperty('value');

      // eslint-disable-next-line no-await-in-loop
      const courtComplexOptionJson = await courtComplexOptionValue.jsonValue();

      if (courtComplexOptionJson !== '0') {
        const firstAt = courtComplexOptionJson.indexOf('@');

        if (firstAt === -1) {
          throw new Error('@ not present in Court Complex Value, Please check... cannot proceed further');
        }
        // One or more courts are merged in court_complexes
        const secondAt = courtComplexOptionJson.lastIndexOf('@');
        const courtIds = courtComplexOptionJson.substring(firstAt + 1, secondAt);
        const courtIdArray = courtIds.split(',');
        for (let cid = 0; cid < courtIdArray.length; cid += 1) {
          if (courtIdArray[cid] === courtId) {
            // eslint-disable-next-line no-await-in-loop
            await page.select('select#court_complex_code', courtComplexOptionJson);
            // eslint-disable-next-line no-await-in-loop
            courtComplexCode = await page.evaluate((el) => el.textContent, courtComplexOption);
            // eslint-disable-next-line no-await-in-loop
            await page.waitFor(800);
            selected = true;
            break;
          }
        }
      }
    }
  } catch (error) {
    if (error.message.includes('not present in Court Complex Value, Please check')) {
      throw new Error(error);
    }
    const message = 'E-CauseList Based Crawling: select#court_complex_code -> Selecting -> Error -> Reloading...';
    throw new Error(message);
  }
  if (!selected) {
    const message = 'E-CauseList Based Crawling: Court complex not selected -> Reloading...';
    throw new Error(message);
  }
  return courtComplexCode;
}

async function selectDropdown(page, value, id, message) {
  try {
    const dropdownOption = (await page.$x(
      `//select[@id = "${id}"]/option[text() = "${value}"]`,
    ))[0];
    const dropDownValue = await (await dropdownOption.getProperty('value')).jsonValue();

    await page.select(`select#${id}`, dropDownValue);
  } catch (error) {
    throw new Error(message);
  }
}

async function clickSelector(page, selector, message) {
  try {
    await page.click(selector);
  } catch (error) {
    throw new Error(message);
  }
}

async function getAllCourtNames(page, courtId) {
  const courtNames = [];
  let courtNameOptions = [];
  let loopWait = 6;
  let message = '';
  await page.waitFor(1300);

  try {
    // if Courts are not loaded, so we are waiting for options to get load.
    while (courtNameOptions.length <= 1 && loopWait) {
      // eslint-disable-next-line no-await-in-loop
      courtNameOptions = await page.$$('#CL_court_no > option');

      loopWait -= 1;
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(500);
    }
    // if court names are not loaded
    if (courtNameOptions.length === 0) {
      await page.waitFor(1000);
      throw new Error('Court Names not loaded');
    }
  } catch (error) {
    message = 'E-CauseList Based Crawling: select#CL_court_no -> Options loading -> error -> Reloading...';
    await page.waitFor(1000);
    throw new Error(message);
  }

  try {
    for (let itr = 0; itr < courtNameOptions.length; itr += 1) {
      // eslint-disable-next-line no-await-in-loop
      await page.waitForSelector(`#CL_court_no > option:nth-child(${itr + 1})`, { timeout: 2000 });

      // eslint-disable-next-line no-await-in-loop
      const courtOption = await page.$(`#CL_court_no > option:nth-child(${itr + 1})`);

      // eslint-disable-next-line no-await-in-loop
      const subCourtName = await page.evaluate((el) => el.textContent, courtOption);

      // Sometimes in courtname options, `there is an error` text is there
      if (subCourtName === 'THERE IS AN ERROR' || subCourtName === 'Select court') {
        message = 'E-CauseList Based Crawling: select#CL_court_no -> Options -> There is an error -> Reloading...';
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(1000);
        throw new Error(message);
      }

      // eslint-disable-next-line no-await-in-loop
      const courtOptionValue = await courtOption.getProperty('value');
      // eslint-disable-next-line no-await-in-loop
      const courtOptionJson = await courtOptionValue.jsonValue();

      // Get court Names from court id.
      // if court id = 3, court name values is starting from 3^
      if (courtOptionJson.startsWith(`${courtId}^`)) {
        courtNames.push(subCourtName);
      }
    }
  } catch (error) {
    message = 'E-CauseList Based Crawling: select#CL_court_no -> Options not loaded -> error -> Reloading...';
    await page.waitFor(1000);
    throw new Error(message);
  }
  return courtNames;
}

async function assignDate(page, eCauseListDate) {
  const message = 'E-CauseList Based Crawling: input#causelist_date -> Assigining -> ecauselist date -> error -> Reloading...';
  try {
    await waitingForSelector(page, 'input#causelist_date', message, 4300);
    await page.$eval('input#causelist_date', (ele, date) => {
    // eslint-disable-next-line no-param-reassign
      ele.value = date;
    }, eCauseListDate);
  } catch (error) {
    throw new Error(message);
  }
}

async function getCases(page, workerId) {
  let message = '';
  let casesPresent = true;

  try {
    await page.waitForSelector('div > table[class = "table table-bordered newtblclr"] > tbody > tr', { timeout: 15000 });
  } catch (error) {
    try {
      await page.waitForSelector('#divWait', { visible: true, timeout: 2000 });
      // if cases are not loaded after clicking on criminal button
      message = `E-CauseList Based Crawling: ${workerId}- Criminal Button Clicked, but cases not loaded`;
      throw new Error(message);
    } catch (err) {
      if (err.message.includes('Criminal Button Clicked')) {
        throw new Error(err);
      }
      casesPresent = false;
    }
  }

  if (!casesPresent) {
    console.log(`E-CauseList Based Crawling: ${workerId}- No cases available for this courtType`);
    return [];
  }

  await page.waitFor(300);
  const criminalCases = await page.$$('div > table[class = "table table-bordered newtblclr"] > tbody > tr > td > a');
  return criminalCases;
}

async function getSelectorValue(page, selector) {
  let document = '';
  try {
    const value = await page.evaluate((docSelector) => document.querySelector(
      docSelector,
    ).textContent, selector);
    document = '';
    return value;
  } catch (error) {
    return '';
  }
}

async function storeHtmlContent(page, cinnumber) {
  let document = '';
  // Storing HTML content of case by cinnumber.html
  const htmlContent = await page.evaluate(() => document.querySelector('*').outerHTML);
  document = '';
  const folderPath = path.join(__dirname, 'html-contents');
  if (!fs.existsSync(folderPath)) {
    fs.mkdirSync(folderPath);
    console.log('E-CauseList Based Crawling: html-contents directory created');
  }
  await page.waitFor(200);
  fs.writeFileSync(path.join(folderPath, `${cinnumber}.html`), htmlContent, 'utf8');
}

async function getCaseDetails(
  page,
  criminalCase,
  recordValues,
  state,
) {
  const subRec = []; // To update in SQL
  let message = '';

  try {
    await page.evaluate((element) => {
      element.click();
    }, criminalCase);

    console.log('E-CauseList Based Crawling: case clicked');
  } catch (error) {
    message = 'E-CauseList Based Crawling: Error -> case -> click -> reloading...';
    throw new Error(message);
  }
  await page.waitFor(300);

  // Back Button will appear when will click on case
  try {
    await page.waitForSelector('div#backTopDiv', { visible: true, timeout: 15000 });
  } catch (error) {
    message = 'E-CauseList Based Crawling: case not loaded properly- crawling again...';
    throw new Error(message);
  }
  await page.waitFor(200);

  try {
    await page.waitForSelector('table > tbody > tr > td > span', { visible: true, timeout: 3000 });
  } catch (error) {
    message = 'E-CauseList Based Crawling: Cinnumber selector not visible -> Reloading...';
    throw new Error(message);
  }

  const cinnumber = await getSelectorValue(page, 'table > tbody > tr > td > span');
  if (cinnumber.length !== 16) {
    message = 'E-CauseList Based Crawling: Cinnumber -> Not correct -> Reloading...';
    throw new Error(message);
  }

  console.log(`E-CauseList Based Crawling: Cinnumber: ${cinnumber}`);

  subRec.push(state);
  subRec.push(`${cinnumber}.html`);
  recordValues.push(subRec);
  await page.waitFor(300);

  await storeHtmlContent(page, cinnumber);

  console.log('E-CauseList Based Crawling: ***********');
  await page.waitFor(500);

  try {
    await page.waitForSelector('input#bckbtn', { timeout: 1000 });
    await page.click('input#bckbtn');
    await page.waitForSelector('div#backTopDiv', { visible: false, timeout: 4000 });
  } catch (error) {
    message = 'E-CauseList Based Crawling: div#backTopDiv -> waiting -> error -> reloading...';
    throw new Error(message);
  }
}

async function enterCaptchaAndClickCriminal(page, eCauseListDate, workerId) {
  let retryCaptcha = true;
  let message = '';
  let caseCaptcha;
  const document = '';
  // retry entering captcha for 4 times -> reload page and generate new captcha
  let retryCount = 4;
  while (retryCaptcha) {
    if (retryCount === 0) {
      message = `E-CauseList Based Crawling: ${workerId}- Reloading page to generate new captcha`;
      throw new Error(message);
    }

    retryCount -= 1;

    try {
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(300);

      // eslint-disable-next-line no-await-in-loop
      caseCaptcha = await getCaptcha(page);

      // Typing Captcha
      // eslint-disable-next-line no-await-in-loop
      await page.type('#captcha', caseCaptcha, { delay: 900 });
    } catch (error) {
      message = `E-CauseList Based Crawling: ${workerId}- Error -> Enter captcha fail -> reloading...`;
      throw new Error(message);
    }

    // eslint-disable-next-line no-await-in-loop
    await page.waitFor(1000);

    try {
      // Click on Criminal Button
      // eslint-disable-next-line no-await-in-loop
      await page.click('input#butCriminal');
    } catch (error) {
      message = `E-CauseList Based Crawling: ${workerId}- input#butCriminal -> Reloading...`;
      throw new Error(message);
    }

    // eslint-disable-next-line no-await-in-loop
    await page.waitFor(2000);

    try {
      // eslint-disable-next-line no-await-in-loop
      await page.waitForSelector('#pageCLDiv', { visible: true }, { timeout: 1700 });
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(700);

      // eslint-disable-next-line no-await-in-loop
      const captchaTextContent = await page.evaluate(() => document.querySelector('div[id = "pageCLDiv"]').textContent);

      if (captchaTextContent === 'Invalid Captcha') {
        console.log(`E-CauseList Based Crawling: ${workerId}- invalid Captcha Entered`);

        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(700);

        // eslint-disable-next-line no-await-in-loop
        await page.waitForSelector('div#backTopCLDiv', { visible: true, timeout: 1000 });

        // eslint-disable-next-line no-await-in-loop
        await page.waitForSelector('div#backTopCLDiv > input#bckbtn', { timeout: 1100 });

        // eslint-disable-next-line no-await-in-loop
        await page.click('div#backTopCLDiv > input#bckbtn');

        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(1000);
        // Selecting date again
        // eslint-disable-next-line no-await-in-loop
        await assignDate(page, eCauseListDate);
      } else {
        // If `Invalid Captcha` is not there
        throw new Error('Not Invalid Captcha in Div');
      }
    } catch (error) {
      try {
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(500);

        // If wrong captcha entered- pop-up will there with OK button
        // eslint-disable-next-line no-await-in-loop
        await page.waitForSelector('#bs_alert', { visible: true, timeout: 150 });
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(100);
        // eslint-disable-next-line no-await-in-loop
        console.log(`E-CauseList Based Crawling: ${workerId}- Wrong Captcha Entered`);
        // Clicking on Pop-Up Ok Button
        // eslint-disable-next-line no-await-in-loop
        await page.click('[class="btn btn-primary btn-sm"]');
        // In this case, we need to reload page to generate new captcha
        retryCount = 0;
        // Selecting date again
        // eslint-disable-next-line no-await-in-loop
        await assignDate(page, eCauseListDate);
      } catch (err) {
        // If Captcha is Correct
        retryCaptcha = false;
      }
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(500);
    }
  }
}

async function moveToHomePage(page, workerId) {
  try {
    await page.waitForSelector('div#backTopCLDiv', { visible: true, timeout: 1900 });
    await page.waitFor(100);
    // above list of all cases-> back button should there
    await page.click('div#backTopCLDiv > input#bckbtn');
    await page.waitForSelector('div#backTopDiv', { visible: false, timeout: 1900 });
    await page.waitForSelector('div#pageCLDiv', { visible: false, timeout: 1900 });
    await page.waitForSelector('div#backTopCLDiv', { visible: false, timeout: 1900 });
  } catch (error) {
    const message = `E-CauseList Based Crawling: ${workerId}- div#backTopCLDiv -> Back button -> Above list of cases -> error -> Reloading...`;
    throw new Error(message);
  }
}

async function startScraping(params) {
  const {
    courtId,
    tableName,
    percentage,
    state,
    district,
    eCauseListDate,
    page,
    workerId,
  } = params;
  let courtComplexCode = '';
  let recordValues = [];
  await page.waitFor(500);
  let looping = true;

  let courtStart = 0;
  let caseStart = 0;

  const requiredValues = {};
  requiredValues.totalCases = 0;
  let message = '';
  let selector = '';
  let timeout = 0;
  while (looping) {
    try {
      // Waiting for selector - Cause List Icon on the side bar
      selector = '.causelist-dp';
      timeout = 11000;
      message = `E-CauseList Based Crawling: ${workerId}- .causelist-dp -> sidebar -> causelist button -> error -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await waitingForSelector(page, selector, message, timeout);

      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(700);

      // Click on Cause List Icon on the side bar
      message = `E-CauseList Based Crawling: ${workerId}- .causelist-dp -> sidebar -> causelist button -> error -> click -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await clickSelector(page, '.causelist-dp', message);

      // Waiting for selector - POP UP with OK button
      message = `E-CauseList Based Crawling: ${workerId}- #bs_alert -> Popup with ok button -> error -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await waitingForSelector(page, '#bs_alert', message, 11000);

      // Waiting for selector - Select State Drop Down
      message = `E-CauseList Based Crawling: ${workerId}- select#sess_state_code -> state code dropdown -> error -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await waitingForSelector(page, 'select#sess_state_code', message, 9000);
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(700);

      message = `E-CauseList Based Crawling: ${workerId}- Popup -> OK button -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await clickSelector(page, '[class="btn btn-primary btn-sm"]', message);
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(500);
      message = `E-CauseList Based Crawling: ${workerId}- Assigining state -> Error -> Reloading...`;
      let dropdownId = 'sess_state_code';
      // eslint-disable-next-line no-await-in-loop
      await selectDropdown(page, state, dropdownId, message);

      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(800);

      // Waiting for selector - Select District Drop Down
      message = `E-CauseList Based Crawling: ${workerId}- select#sess_dist_code -> Select District Drop-down -> error -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await waitingForSelector(page, 'select#sess_dist_code', message, 6500);
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(1300);

      // Assigning District
      dropdownId = 'sess_dist_code';
      message = `E-CauseList Based Crawling: ${workerId}- Assigining -> district -> error -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await selectDropdown(page, district, dropdownId, message);

      message = `E-CauseList Based Crawling: ${workerId}- select#court_complex_code -> court options -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await waitingForSelector(page, 'select#court_complex_code', message, 9000);

      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(1000);

      // eslint-disable-next-line no-await-in-loop
      courtComplexCode = await selectCourtComplex(page, courtId);

      message = `E-CauseList Based Crawling: ${workerId}- select#CL_court_no -> Reloading...`;
      // eslint-disable-next-line no-await-in-loop
      await waitingForSelector(page, 'select#CL_court_no', message, 8000);
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(1800);

      // eslint-disable-next-line no-await-in-loop
      const courtNames = await getAllCourtNames(page, courtId);

      let lastCourtCases = -1;
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(500);
      console.log(`E-CauseList Based Crawling: ${workerId}- Starting from court index: ${courtStart + 1}`);

      if (courtStart >= courtNames.length) {
        message = `E-CauseList Based Crawling: ${workerId}- Court Names not loaded properly... -> Reloading...`;
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(900);
        throw new Error(message);
      }
      while (courtStart < courtNames.length) {
        const court = courtNames[courtStart];
        dropdownId = 'CL_court_no';
        message = `E-CauseList Based Crawling: ${workerId}- Assigining -> Court -> select#CL_court_no -> error -> Reloading...`;
        // eslint-disable-next-line no-await-in-loop
        await selectDropdown(page, court, dropdownId, message);
        console.log(`E-CauseList Based Crawling: ${workerId}- ----${state}----`);
        console.log(`E-CauseList Based Crawling: ${workerId}- ----${district}----`);
        console.log(`E-CauseList Based Crawling: ${workerId}- ----${courtComplexCode}----`);
        console.log(`E-CauseList Based Crawling: ${workerId}- ----${court}----`);
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(700);

        // eslint-disable-next-line no-await-in-loop
        await assignDate(page, eCauseListDate);
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(500);

        message = `E-CauseList Based Crawling: ${workerId}- input#butCriminal -> Waiting for Criminal button -> error -> Reloading...`;
        // eslint-disable-next-line no-await-in-loop
        await waitingForSelector(page, 'input#butCriminal', message, 5500);

        // eslint-disable-next-line no-await-in-loop
        await enterCaptchaAndClickCriminal(page, eCauseListDate, workerId);

        // eslint-disable-next-line no-await-in-loop
        const criminalCases = await getCases(page, workerId);

        if (criminalCases.length !== 0) {
          // eslint-disable-next-line no-await-in-loop
          await page.waitFor(600);
          const perc = parseFloat(percentage, 10);
          console.log(`E-CauseList Based Crawling: ${workerId}- Total cases are ${criminalCases.length}`);
          console.log(`E-CauseList Based Crawling: ${workerId}- Checking ${percentage}% cases`);
          console.log(`E-CauseList Based Crawling: ${workerId}- Total cases for testing: ${Math.floor((criminalCases.length / 100) * perc)}`);

          // Sometime, after selecting different court, cases of last court appear.
          if (lastCourtCases === criminalCases.length) {
            message = `E-CauseList Based Crawling: ${workerId}- Court cases not loaded properly, crawling again...`;
            throw new Error(message);
          }

          lastCourtCases = criminalCases.length;

          // eslint-disable-next-line no-await-in-loop
          await page.waitFor(900);

          // eslint-disable-next-line no-await-in-loop
          if (Math.floor((criminalCases.length / 100) * perc) > 0) {
            console.log(`E-CauseList Based Crawling: Starting from case: ${workerId}-  ${caseStart + 1}`);
          }

          while (caseStart < Math.floor((criminalCases.length / 100) * perc)) {
            console.log(`E-CauseList Based Crawling: ${workerId}- ${caseStart + 1}/${Math.floor((criminalCases.length / 100) * perc)}`);
            // eslint-disable-next-line no-await-in-loop
            await page.waitFor(500);
            // eslint-disable-next-line no-await-in-loop
            await getCaseDetails(
              page,
              criminalCases[caseStart],
              recordValues,
              state,
            );
            caseStart += 1;
          }
          requiredValues.totalCases += caseStart;
          caseStart = 0;

          if (recordValues.length > 0) {
            const query = `insert into ${database}.${tableName} (state, fileName) values ?`;
            // eslint-disable-next-line no-await-in-loop
            await localDb.insertMultipleRows(query, recordValues);
            console.log(`E-CauseList Based Crawling: ${workerId}- Records inserted in ${tableName}`);
          }
        }
        recordValues = [];
        caseStart = 0;
        // eslint-disable-next-line no-await-in-loop
        await moveToHomePage(page, workerId);
        courtStart += 1;
      }
      looping = false;
    } catch (error) {
      try {
        if (error.message.includes('cannot proceed further')) {
          requiredValues.error = error.message;
          return requiredValues;
        }
        // eslint-disable-next-line no-await-in-loop
        await pageReload(page, error.message, workerId);
      } catch (err) {
        if (err.message.includes('cannot proceed further')) {
          requiredValues.error = err.message;
          return requiredValues;
        }
        throw new Error(error);
      }
    }
  }
  return requiredValues;
}

async function openPage(values) {
  const params = values;

  let looping = true;
  let page;
  let browser;
  let browserOpenRetries = 5;
  while (looping && browserOpenRetries > 0) {
    try {
      // eslint-disable-next-line no-await-in-loop
      browser = await puppeteer.launch({ headless: true, slowMo: 11, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
      break;
    } catch (error) {
      browserOpenRetries -= 1;
      console.log(error);
      console.log(`E-CauseList Based Crawling: ${params.workerId}- Browser open Retries: ${browserOpenRetries}`);
    }
  }
  if (browserOpenRetries === 0) {
    console.log(`E-CauseList Based Crawling: ${params.workerId}- Not able to launch browser...`);
  }
  // let browser = await puppeteer.launch();
  let requiredValues = {};
  while (looping) {
    try {
      // eslint-disable-next-line no-await-in-loop
      page = await browser.newPage();

      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(1000);

      // eslint-disable-next-line no-await-in-loop
      await page.goto('https://services.ecourts.gov.in/ecourtindia_v6/', { waitUntil: 'networkidle0' });
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(1400);
      break;
    } catch (error) {
      if (error.message.includes('ERR_INTERNET_DISCONNECTED')) {
        console.log(`E-CauseList Based Crawling: ${params.workerId}- Please check your internet connection...`);
        console.log(`E-CauseList Based Crawling: ${params.workerId}- retrying in 3 min`);
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(180000);
      } else {
        console.log(error);
        console.log(`E-CauseList Based Crawling: ${params.workerId}- openPage: ecourts.gov server down`);
      }
      maxReloadCount -= 1;
      if (!maxReloadCount) {
        requiredValues.error = 'Max reload count reached... cannot proceed further';
        // eslint-disable-next-line no-await-in-loop
        await browser.close();
        return requiredValues;
      }
      if (page) {
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(1400);
      }
      // eslint-disable-next-line no-await-in-loop
      await browser.close();
      if (page) {
        // eslint-disable-next-line no-await-in-loop
        await page.waitFor(3000);
      }
      // eslint-disable-next-line no-await-in-loop
      browser = await puppeteer.launch({ headless: true, slowMo: 11, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
      // eslint-disable-next-line no-await-in-loop
      // browser = await puppeteer.launch();
    }
  }
  looping = true;
  while (looping) {
    try {
      params.page = page;
      // eslint-disable-next-line no-await-in-loop
      requiredValues = await startScraping(params);
      // eslint-disable-next-line no-await-in-loop
      await browser.close();

      if (requiredValues.error && requiredValues.error.includes('cannot proceed further')) {
        // eslint-disable-next-line no-await-in-loop
        await browser.close();
        return requiredValues;
      }

      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(1900);
      looping = false;
    } catch (error) {
      console.log(error);
      // eslint-disable-next-line no-await-in-loop
      await pageReload(page, error.message, params.workerId);
      // eslint-disable-next-line no-await-in-loop
      await page.waitFor(3400);
    }
  }
  return requiredValues;
}

async function mergeRecordsToMainTable(requiredValues, dataValues) {
  const stateAbbrevation = constants.getStandardTableName(dataValues.state);
  let query;
  // if last id is not in map - insert with lastId
  // and in other map add total number of crawled records
  if (!crawlingLastIdDetails.has(stateAbbrevation)) {
    query = `create table IF NOT EXISTS ${database}.${stateAbbrevation}_crawl (id int NOT NULL AUTO_INCREMENT,fileName varchar(200), state varchar(100), parsed enum ('not parsed', 'parsed', 'file not found', 'parsing error') DEFAULT 'not parsed', PRIMARY KEY (id))`;
    // eslint-disable-next-line no-await-in-loop
    await localDb.executeQuerySync(query);
    query = `select id from ${database}.${stateAbbrevation}_crawl ORDER BY ID DESC LIMIT 1`;
    // eslint-disable-next-line no-await-in-loop
    let lastId = await localDb.executeQuerySync(query);
    if (lastId.length === 0) {
      lastId = 0;
    } else {
      lastId = lastId[0].id;
    }
    crawlingLastIdDetails.set(stateAbbrevation, lastId);
  }
  if (!stateTotalCases.has(stateAbbrevation)) {
    stateTotalCases.set(stateAbbrevation, requiredValues.totalCases);
  } else {
    const crawledCases = stateTotalCases.get(stateAbbrevation);
    stateTotalCases.set(stateAbbrevation, crawledCases + requiredValues.totalCases);
  }
  console.log('Total Cases Crawled Map');
  console.log(stateTotalCases);
  console.log('State Last ID Map');
  console.log(crawlingLastIdDetails);
  query = `insert into ${database}.${stateAbbrevation}_crawl (fileName,state, parsed) select fileName,state, parsed from ${dataValues.tableName};`
  await localDb.executeQuerySync(query);
  console.log(`records merge to main table : ${stateAbbrevation}_crawl`);
  query = `Drop table ${dataValues.tableName};`;
  await localDb.executeQuerySync(query);
  console.log(`${dataValues.tableName} Table dropped...`);
}

async function addTasksToQueueAndProcess(perc, eCauseListDate) {
  let district;
  let courtId;

  let query = `select distinct(stateName) from ${database}.ecourt_court_list where active=1`;
  const activeStates = await localDb.executeQuerySync(query);
  for (let st = 0; st < activeStates.length; st += 1) {
    const state = activeStates[st].stateName;
    query = `select * from ${database}.ecourt_court_list where active=1 and stateName = "${state}"`;
    // eslint-disable-next-line no-await-in-loop
    const activeCourts = await localDb.executeQuerySync(query);

    for (let i = 0; i < activeCourts.length; i += 1) {
      const tableName = `${constants.getStandardTableName(state)}_${i + 1}`;
      query = `create table IF NOT EXISTS ${database}.${tableName} (id int NOT NULL AUTO_INCREMENT,fileName varchar(200), state varchar(100), parsed enum ('not parsed', 'parsed', 'file not found', 'parsing error') DEFAULT 'not parsed', PRIMARY KEY (id))`;
      // eslint-disable-next-line no-await-in-loop
      await localDb.executeQuerySync(query);
      console.log(`E-CauseList Based Crawling: Table Name is: ${tableName}`);
      district = activeCourts[i].distName;
      courtId = activeCourts[i].courtId;
      const values = {};
      values.courtId = courtId;
      values.tableName = tableName;
      values.percentage = perc;
      values.state = state;
      values.district = district;
      values.eCauseListDate = eCauseListDate;
      query = `select id from ${database}.${tableName} ORDER BY ID DESC LIMIT 1`;
      // eslint-disable-next-line no-await-in-loop
      let lastId = await localDb.executeQuerySync(query);
      if (lastId.length === 0) {
        lastId = 0;
      } else {
        lastId = lastId[0].id;
      }
      values.tempTableLastId = lastId;
      console.log(values);
      cluster.queue({ values });
    }
  }
}

async function initScraping(perc, eCauseListDate) {
  reloadCounts = new Map();
  maxReloadCounts = new Map();
  crawlingLastIdDetails = new Map();
  stateTotalCases = new Map();
  let clusterDeclarationRetry = 5;
  const looping = true;
  while (looping && clusterDeclarationRetry) {
    try {
      if (cluster === undefined) {
        // eslint-disable-next-line no-await-in-loop
        cluster = await Cluster.launch({
          concurrency: Cluster.CONCURRENCY_CONTEXT,
          maxConcurrency: Number.parseInt(process.env.CRAWLING_PUPPETEER_INSTANCES, 10),
          // monitor: true,
          timeout: 86400000, // Specify a timeout for all tasks
          retryLimit: 10,
          retryDelay: 1000,
          // skipDuplicateUrls: true,
          workerCreationDelay: 10000, // delay in launching multiple workers
          puppeteerOptions: { headless: true, slowMo: 11, args: ['--no-sandbox', '--disable-setuid-sandbox'] },
        });
      }
      break;
    } catch (error) {
      console.log(error);
      clusterDeclarationRetry -= 1;
    }
  }

  cluster.on('taskerror', async (err, data, willRetry) => {
    console.log(err);
    if (willRetry) {
      console.warn(`Encountered an error while crawling ${data}. ${err.message}\nThis job will be retried`);
    } else {
      console.error(`Failed to crawl ${data}: ${err.message}`);
      console.log(err);
    }
  });

  await cluster.task(async ({ page, data, worker }) => {
    const dataValues = data.values;
    dataValues.workerId = worker.id;
    console.log('Crawling started for' || page);
    console.log(dataValues);
    reloadCount = parseInt(process.env.E_CAUSELIST_RELOAD_COUNT, 10) || 100;
    maxReloadCount = parseInt(process.env.E_CAUSELIST_MAX_RELOAD_COUNT, 10) || 1000;
    reloadCounts.set(worker.id, reloadCount);
    maxReloadCounts.set(worker.id, maxReloadCount);
    const requiredValues = await openPage(dataValues);
    if (requiredValues.error) {
      console.log(`E-CauseList Based Crawling: ${worker.id}- ${requiredValues.error}`);
    }
    await mergeRecordsToMainTable(requiredValues, dataValues);
    console.log(`${worker.id}- ****************Job is completed****************`);
  });
  await addTasksToQueueAndProcess(perc, eCauseListDate);
  await cluster.idle();
  const crawledStateDetails = [];
  for (const [key, value] of crawlingLastIdDetails.entries()) {
    crawledStateDetails.push({state: key, lastId: value, totalCrawledCases: stateTotalCases.get(key)})
    console.log(key, value);
  }
  await sendMail.eCauseListCrawlingMail({ crawledStateDetails, eCauseListDate, percentageCheck: perc });
  await cluster.close();
}

module.exports.initScraping = initScraping;
