const express = require('express');
// const debug = require('debug')('E-CauseList-Crawler');

const router = express.Router();
const request = require('request');

const eCauseListCrawlingSheetUpdate = require('./ecause-list-parsing-sheet-update');
const causeListCrawling = require('./e-cause-list-crawling');
const causeListParsing = require('./e-cause-list-parsing');

const sendEmail = require('./sendMail');

const currentTestsTracker = require('../current-tests-tracker');
const constants = require('./constants');

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'E-CauseList-Crawler', CURRENT_TESTS: currentTestsTracker.CURRENT_TESTS });
});

router.get('/eCauseListCrawling', async (req, res) => {
  res.render('e-causelist-crawling');
});

async function startCauseListCrawling(perc, eCauseListDate) {
  const d = new Date();
  const testId = d.getTime();
  currentTestsTracker.addToTracking(testId, 'E-Cause list Crawling');
  await causeListCrawling.initScraping(perc, eCauseListDate);

  currentTestsTracker.removeFromTracking(testId);
}

router.post('/eCauseListCrawling', async (req, res) => {
  res.redirect('/');
  const { eCauseListDate } = req.body;
  const perc = req.body.percentage;
  let date = `${eCauseListDate.substr(8, 2)}-`;
  date += `${eCauseListDate.substr(5, 2)}-`;
  date += `${eCauseListDate.substr(0, 4)}`;
  await startCauseListCrawling(perc, date);
});

router.post('/eCauseListBasedParsing', async (req, res) => {
  const d = new Date();
  const testId = d.getTime();
  currentTestsTracker.addToTracking(testId, 'E-Cause list Parsing');
  const dashboard = req.param('dashboard', null);
  res.send('E-CauseList Based Parsing Started');
  const sheetUpdatedValues = await causeListParsing.initParsing(dashboard);
  const params = {};
  params.sheetUpdatedValues = sheetUpdatedValues;
  params.mailHeading = 'E-CauseList Parsing results';
  params.eCauseListTestingDb = process.env.E_CAUSELIST_TESTING_DB;
  params.emailReceivers = process.env.E_CAUSELIST_EMAIL_RECIEVERS;
  params.gDocumentId = process.env.E_CAUSELIST_TEST_DOCUMENT_ID;
  params.gSheetId = process.env.E_CAUSELIST_TEST_SHEET_ID;
  params.ejsPath = 'e-causelist-parsing';
  sendEmail.eCauseListParsingMail(params, testId);
  await eCauseListCrawlingSheetUpdate.exportDataToDrive(
    testId,
    params.sheetUpdatedValues,
    params.eCauseListTestingDb,
  );
  currentTestsTracker.removeFromTracking(testId);

  let tables = '';
  let lastRecordIds = '';
  let stateAbbreviation = '';
  const requiredTablesToSync = [];
  for (let i = 0; i < sheetUpdatedValues.length; i += 1) {
    const {
      tableName,
      missingCases,
      missingRecordsTable,
      missingTableLastid,
    } = sheetUpdatedValues[i];
    if (missingCases && missingCases > 0) {
      requiredTablesToSync.push({ tableName, missingRecordsTable, missingTableLastid });
    }
  }
  for (let i = 0; i < requiredTablesToSync.length; i += 1) {
    const {
      tableName,
      missingRecordsTable,
      missingTableLastid,
    } = requiredTablesToSync[i];
    if (i !== 0) {
      tables += ',';
      lastRecordIds += ',';
      stateAbbreviation += ',';
    }
    tables += missingRecordsTable;
    lastRecordIds += missingTableLastid.toString();
    stateAbbreviation += tableName;
  }
  if (tables !== '') {
    console.log('Syncing missing records to ES:-');
    console.log('Tables: ', tables);
    console.log('lastRecordIds: ', lastRecordIds);
    console.log('stateAbbreviation: ', stateAbbreviation);
    const options = {
      method: 'POST',
      url: constants.AUTO_SYNC_API,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Basic ${process.env.AUTO_SYNC_AUTH_TOKEN}`,
      },
      form: {
        sourceMachineId: constants.DATA_SOURCE_MACHINES.DEV_2.id, // Dev2
        tables,
        database: process.env.E_CAUSELIST_TESTING_DB,
        courtType: 'Dc_EcauseList',
        lastRecordIds,
        stateAbbreviation,
      },
    };
    console.log(options);
    request(options, (error, response) => {
      if (error) throw new Error(error);
      console.log(response.body);
    });
  } else {
    console.log('No missing data to sync');
  }
});

module.exports = router;
