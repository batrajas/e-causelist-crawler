require('dotenv').config();

const { AUTO_SYNC_API } = process.env;

const getDashBoardValue = function getDashBoardValue(dashboard) {
  switch (dashboard) {
    case 'internal':
      return 'Production';
    case 'production':
      return 'Production';
    case 'Dev2 Cloud':
      return 'Dev2 Cloud';
    case 'probe_dev':
      return 'Probe-dev';
    case 'devMachine':
      return 'Dev';
    case 'uat':
    return 'uat';
    default:
      return '';
  }
};

function getStandardTableName(stateName) {
  const state = stateName.toLowerCase();
  switch (state) {
    case 'andaman and nicobar':
      return 'AN';
    case 'dnh at silvasa':
      return 'DNH';
    case 'assam':
      return 'AM';
    case 'andhra pradesh':
      return 'AP';
    case 'bihar':
      return 'BR';
    case 'chattisgarh':
      return 'CG';
    case 'chandigarh':
      return 'CH';
    case 'delhi':
      return 'DL';
    case 'diu and daman':
      return 'DN';
    case 'goa':
      return 'GA';
    case 'gujarat':
      return 'GJ';
    case 'himachal pradesh':
      return 'HP';
    case 'haryana':
      return 'HR';
    case 'jharkhand':
      return 'JH';
    case 'jammu and kashmir':
      return 'JK';

    case 'karnataka':
      return 'KA';
    case 'kerala':
      return 'KL';
    case 'ladakh':
      return 'LK';
    case 'meghalaya':
      return 'MG';
    case 'maharashtra':
      return 'MH';
    case 'manipur':
      return 'MN';
    case 'madhya pradesh':
      return 'MP';
    case 'mizoram':
      return 'MZ';
    case 'nagaland':
      return 'NG';
    case 'orissa':
      return 'OD';
    case 'punjab':
      return 'PB';
    case 'rajasthan':
      return 'RJ';
    case 'sikkim':
      return 'SK';

    case 'tamil nadu':
      return 'TN';
    case 'puducherry':
      return 'PD';
    case 'tripura':
      return 'TR';
    case 'telangana':
      return 'TL';
    case 'uttarakhand':
      return 'UK';
    case 'uttar pradesh':
      return 'UP';
    case 'west bengal':
      return 'WB';
    // no default
  }
  return 'invalid';
}

const getEsProxyApiKey = function getEsProxyApiKey(server) {
  switch (server) {
    case 'Production':
      return process.env.ES_PROXY_PRODUCTION_API_KEY;
    case 'Dev2':
      return process.env.ES_PROXY_DEV2_API_KEY;
    case 'Dev':
      return process.env.ES_PROXY_DEV_API_KEY;
    case 'Probe-dev':
      return process.env.ES_PROXY_PROBE_DEV_API_KEY;
    case 'UAT':
      return process.env.ES_PROXY_UAT_API_KEY;
    case 'uat':
      return process.env.ES_PROXY_UAT_API_KEY;
    default:
      return process.env.ES_PROXY_API_KEY;
  }
};

const getHostUrl = function getHostUrl(server) {
  switch (server) {
    case 'Production':
      return 'http://es-proxy.getupforchange.com/executeQuery';
    case 'Dev2 Cloud':
      return 'http://dev2-cloud-es-proxy.getupforchange.com/executeQuery';
    case 'Probe-dev':
      return 'http://probe-dev-es-proxy.getupforchange.com/executeQuery';
    case 'Dev':
      return 'http://dev-es-proxy.getupforchange.com/executeQuery';
    case 'UAT':
      return 'http://uat-es-proxy.getupforchange.com/executeQuery';
    case 'uat':
      return 'http://uat-es-proxy.getupforchange.com/executeQuery';
    default:
      return '';
  }
};

const DATA_SOURCE_MACHINES = {
  CRAWL_MACHINE: {
    id: 1,
    name: 'Crawl Machine',
  },
  ID_MACHINE: {
    id: 2,
    name: 'ID Machine',
  },
  DEV2_CLOUD: {
    id: 3,
    name: 'Dev2 Cloud Machine',
  },
  E2EML: {
    id: 4,
    name: 'E2E ML Machine',
  },
  LocalHost: {
    id: 5,
    name: 'LocalHost',
  },
  SHARED_MYSQL_MACHINE: {
    id: 6,
    name: 'My SQL Machine',
  },
  DEV_2: {
    id: 7,
    name: 'Dev2 Machine',
  },
};

module.exports = {
  getDashBoardValue,
  getStandardTableName,
  getEsProxyApiKey,
  getHostUrl,
  DATA_SOURCE_MACHINES,
  AUTO_SYNC_API,
};
