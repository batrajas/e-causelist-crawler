require('dotenv').config();

module.exports = {

  serverName: 'E-CauseList-Crawler',

  path: {
    json: `${__dirname}/../json`,
  },
};
